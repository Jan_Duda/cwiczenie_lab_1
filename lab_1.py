from scipy.stats import norm
from csv import writer
import numpy as np

def generate_points_poz(num_points:int, szerokosc, dlugosc):
    distribution_x = norm(loc=0, scale=szerokosc)
    distribution_y = norm(loc=0, scale=dlugosc)
    distribution_z = norm(loc=0, scale=0)

    x = distribution_x.rvs(size=num_points)
    y = distribution_y.rvs(size=num_points)
    z = distribution_z.rvs(size=num_points)

    points = zip(x, y, z)
    return points


def generate_points_pion(num_points:int, szerokosc, wysokosc):
    distribution_x = norm(loc=0, scale=szerokosc)
    distribution_y = norm(loc=0, scale=0)
    distribution_z = norm(loc=0, scale=wysokosc)

    x = distribution_x.rvs(size=num_points)
    y = distribution_y.rvs(size=num_points)
    z = distribution_z.rvs(size=num_points)

    points = zip(x, y, z)
    return points


def generate_points_cylin(num_points:int, promien, wysokosc):

    u = np.linspace(0, 2 * np.pi, num_points)
    x = promien * np.cos(u)
    y = promien * np.sin(u)


    distribution_z = norm(loc=0, scale=wysokosc)
    z = distribution_z.rvs(size=num_points)

    points = zip(x, y, z)
    return points


if __name__ == '__main__':
    cloud_points_poz = generate_points_poz(3000, 500, 500)
    with open('poz.xyz', 'w', encoding='utf-8', newline='\n') as csvfile:
        csvwriter = writer(csvfile)
        for p in cloud_points_poz:
            csvwriter.writerow(p)


    cloud_points_pion = generate_points_pion(3000, 500, 1000)
    with open('pion.xyz', 'w', encoding='utf-8', newline='\n') as csvfile:
        csvwriter = writer(csvfile)
        for p in cloud_points_pion:
            csvwriter.writerow(p)


    cloud_points_cylin = generate_points_cylin(10000, 500, 1000)
    with open('cylin.xyz', 'w', encoding='utf-8', newline='\n') as csvfile:
        csvwriter = writer(csvfile)
        for p in cloud_points_cylin:
            csvwriter.writerow(p)